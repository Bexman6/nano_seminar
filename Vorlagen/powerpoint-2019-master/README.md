In diesem Repository sind Vorlagen für eine Powerpoint Präsentation in 4:3 und 16:9 Format, sowie eine Postervorlage A0 enthalten.
Die Schriftart Myriad Pro sollte für die Verwendung installiert sein. 
Format und allg. Aussehen orientieren sich bis auf einige Kleinigkeiten an den Latex-Vorlagen.
